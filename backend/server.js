import express from 'express';
import cors from 'cors';
import connectDB from './config/db.js';
import dotenv from 'dotenv';

import productRoutes from './routes/productRoutes.js';
import userRoutes from './routes/userRoutes.js';
import orderRoutes from './routes/orderRoutes.js';
import {notFound, errorHandler} from './middleware/errorMiddleware.js';

dotenv.config();

connectDB();

const app = express();

app.use(express.json());
app.use(cors());

app.get("/", (req,res) => {
    res.send("API is running");
});

app.use("/api/products", productRoutes);

app.use("/api/users", userRoutes);

app.use('/api/orders',orderRoutes);

app.use('/api/config/paypal', (req, res) => res.send(process.env.PAYPAL_CLIENT_ID));

<<<<<<< HEAD
const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));


=======
>>>>>>> parent of de85bb4 (Project Version-1 Complete)
app.use(notFound);

app.use(errorHandler);


const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server running at PORT: ${PORT}`);
});