# MERN-eCommerce-shop

Technologies used - React, MongoDB, express.js, node.js.

The store has functionalities for buying products, payment gateway, user-authentication, product search and rating and admin functions to manage the store.

The store has User authentication and authorization using JSON web-tokens (JWT) and Application state management is done using redux and the app has Payment gateway implemented using PayPal.

Project Demo [https://game-mart-42862.web.app/](https://game-mart-42862.web.app/)
