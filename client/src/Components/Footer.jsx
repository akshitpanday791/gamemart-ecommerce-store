import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';

const Footer = () => {
    return (
        <footer>
            <Container>
                <Row>
                    <Col className="text-center py-3">
                        Copyright &copy; GameMart (Project by <a href="https://iamakshit.tk/" target="__blank"><u><b>Akshit Panday</b></u> </a>)
                    </Col>
                </Row>
            </Container>
        </footer>
    );
};

export default Footer;
