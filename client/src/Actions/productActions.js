import axios from 'axios';
import {PRODUCT_LIST_REQUEST, PRODUCT_LIST_SUCCESS, PRODUCT_LIST_FAIL, PRODUCT_DELETE_FAIL, PRODUCT_DELETE_SUCCESS, PRODUCT_DELETE_REQUEST} from '../Constants/productConstants';
import {PRODUCT_DETAILS_REQUEST, PRODUCT_DETAILS_SUCCESS, PRODUCT_DETAILS_FAIL} from '../Constants/productConstants';
import {PRODUCT_CREATE_FAIL, PRODUCT_CREATE_RESET, PRODUCT_CREATE_SUCCESS, PRODUCT_CREATE_REQUEST} from '../Constants/productConstants';
import {PRODUCT_UPDATE_FAIL, PRODUCT_UPDATE_REQUEST, PRODUCT_UPDATE_SUCCESS} from '../Constants/productConstants';

<<<<<<< HEAD
const URL = 'https://game-mart.herokuapp.com';

export const listProducts = (keyword = '', pageNumber = '') => async (dispatch) => {
    try {
        dispatch({type: PRODUCT_LIST_REQUEST});

        const { data } = await axios.get(`${URL}/api/products?keyword=${keyword}&pageNumber=${pageNumber}`);
=======
export const listProducts = () => async(dispatch) => {
    try {
        dispatch({type: PRODUCT_LIST_REQUEST});

        const{data} = await axios.get('/api/products');
>>>>>>> parent of de85bb4 (Project Version-1 Complete)

        dispatch({type: PRODUCT_LIST_SUCCESS, payload: data});

    } catch (error) {
        dispatch({
            type: PRODUCT_LIST_FAIL, 
            payload: error.response && error.response.data.message? error.response.data.message : error.message
        });
    }
};


export const listProductDetails = (id) => async(dispatch) => {
    try {
        dispatch({type: PRODUCT_DETAILS_REQUEST});

        const{data} = await axios.get(`${URL}/api/products/${id}`);

        dispatch({type: PRODUCT_DETAILS_SUCCESS, payload: data});

    } catch (error) {
        dispatch({
            type: PRODUCT_DETAILS_FAIL, 
            payload: error.response && error.response.data.message? error.response.data.message : error.message
        });
    }
};

export const deleteProduct = (id) => async (dispatch, getState) => {
    try {
      dispatch({type: PRODUCT_DELETE_REQUEST});
  
      const {userLogin: { userInfo }} = getState();
  
      const config = {
        headers: {
          Authorization: `Bearer ${userInfo.token}`,
        },
      };
  
      await axios.delete(`${URL}/api/products/${id}`, config);
  
      dispatch({type: PRODUCT_DELETE_SUCCESS})
    } catch (error) {
      dispatch({
        type: PRODUCT_DELETE_FAIL,
        payload:
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message,
      });
    };
};

export const createProduct = () => async (dispatch, getState) => {
  try {
    dispatch({type: PRODUCT_CREATE_REQUEST});

    const {userLogin: { userInfo }} = getState();

    const config = {
      headers: {
        Authorization: `Bearer ${userInfo.token}`,
      },
    };

    const { data } = await axios.post(`${URL}/api/products`, {}, config);

    dispatch({type: PRODUCT_CREATE_SUCCESS,payload: data});
  } catch (error) {
    dispatch({
      type: PRODUCT_CREATE_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};

export const updateProduct = (product) => async (dispatch, getState) => {
  try {
    dispatch({type: PRODUCT_UPDATE_REQUEST});

    const {
      userLogin: { userInfo },
    } = getState();

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    const { data } = await axios.put(
      `${URL}/api/products/${product._id}`,
      product,
      config
    );

    dispatch({ type: PRODUCT_UPDATE_SUCCESS,payload: data});
  } catch (error) {
    dispatch({
      type: PRODUCT_UPDATE_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
<<<<<<< HEAD
};

export const createProductReview = (productId, review) => async (dispatch,getState) => {
  try {
    dispatch({type: PRODUCT_CREATE_REVIEW_REQUEST});

    const { userLogin: { userInfo }} = getState();

    const config = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${userInfo.token}`,
      },
    }

    await axios.post(`${URL}/api/products/${productId}/reviews`, review, config);

    dispatch({type: PRODUCT_CREATE_REVIEW_SUCCESS});
  } catch (error) {
    dispatch({
      type: PRODUCT_CREATE_REVIEW_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    })
  }
};

export const listTopProducts = () => async (dispatch) => {
  try {
    dispatch({ type: PRODUCT_TOP_REQUEST });

    const { data } = await axios.get(`${URL}/api/products/top`);

    dispatch({
      type: PRODUCT_TOP_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: PRODUCT_TOP_FAIL,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    })
  }
=======
>>>>>>> parent of de85bb4 (Project Version-1 Complete)
};